<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_</name>
   <tag></tag>
   <elementGuidId>15400407-1fa7-4e7f-955e-324204d13d13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_product_dropDownList']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'p$lt$ctl03$pageplaceholder$p$lt$ctl02$KAEForm$viewBiz$product$dropDownList' and @id = 'p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_product_dropDownList' and (text() = '
				กรุณาเลือกประเภทสินเชื่อ
				กรุงศรี คาร์ ฟอร์ แคช
				กรุงศรี คาร์ ฟอร์ แคช โปะ
				กรุงศรี คาร์ ฟอร์ แคช มอเตอร์ไซค์
				กรุงศรี คาร์ ฟอร์ แคช มอเตอร์ไซค์ โปะ
				กรุงศรี นิว คาร์
				กรุงศรี ยูสด์ คาร์
				กรุงศรี รถบ้าน
				กรุงศรี บิ๊ก ไบค์
				กรุงศรี มอเตอร์ไซค์

			' or . = '
				กรุณาเลือกประเภทสินเชื่อ
				กรุงศรี คาร์ ฟอร์ แคช
				กรุงศรี คาร์ ฟอร์ แคช โปะ
				กรุงศรี คาร์ ฟอร์ แคช มอเตอร์ไซค์
				กรุงศรี คาร์ ฟอร์ แคช มอเตอร์ไซค์ โปะ
				กรุงศรี นิว คาร์
				กรุงศรี ยูสด์ คาร์
				กรุงศรี รถบ้าน
				กรุงศรี บิ๊ก ไบค์
				กรุงศรี มอเตอร์ไซค์

			')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>p$lt$ctl03$pageplaceholder$p$lt$ctl02$KAEForm$viewBiz$product$dropDownList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_product_dropDownList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>DropDownField form-control selectpicker</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-98</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				กรุณาเลือกประเภทสินเชื่อ
				กรุงศรี คาร์ ฟอร์ แคช
				กรุงศรี คาร์ ฟอร์ แคช โปะ
				กรุงศรี คาร์ ฟอร์ แคช มอเตอร์ไซค์
				กรุงศรี คาร์ ฟอร์ แคช มอเตอร์ไซค์ โปะ
				กรุงศรี นิว คาร์
				กรุงศรี ยูสด์ คาร์
				กรุงศรี รถบ้าน
				กรุงศรี บิ๊ก ไบค์
				กรุงศรี มอเตอร์ไซค์

			</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_product_dropDownList&quot;)</value>
   </webElementProperties>
</WebElementEntity>
