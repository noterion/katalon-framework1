<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CheckMSG</name>
   <tag></tag>
   <elementGuidId>a4c052be-195a-4974-b991-af0327b83f2d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button'][contains(.,'กรุงศรี มอเตอร์ไซค์')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'กรุงศรี มอเตอร์ไซค์' or . = 'กรุงศรี มอเตอร์ไซค์')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>filter-option pull-left</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>กรุงศรี มอเตอร์ไซค์</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_ncpproduct&quot;)/div[@class=&quot;btn-group bootstrap-select DropDownField form-control&quot;]/button[@class=&quot;btn dropdown-toggle btn-default&quot;]/span[@class=&quot;filter-option pull-left&quot;]</value>
   </webElementProperties>
</WebElementEntity>
