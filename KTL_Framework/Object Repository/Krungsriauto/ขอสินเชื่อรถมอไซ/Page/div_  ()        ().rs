<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_  ()        ()</name>
   <tag></tag>
   <elementGuidId>f60f2d48-89ef-4095-8567-50cb957fa33f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_pnlForm']/div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group disclosure-wrapper row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ความยินยอม
			
		ข้าพเจ้ายินยอมให้ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) และ/หรือ บริษัท อยุธยา แคปปิตอล ออโต้ ลีส จำกัด (มหาชน) ใช้ข้อมูลที่ข้าพเจ้าให้ไว้ข้างต้นในการนำเสนอผลิตภัณฑ์สินเชื่อยานยนต์ และ/หรือ ผลิตภัณฑ์ประกันภัยที่เห็นว่าเป็นประโยชน์แก่ข้าพเจ้าได้
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_pnlForm&quot;)/div[@class=&quot;form-group disclosure-wrapper row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='p_lt_ctl03_pageplaceholder_p_lt_ctl02_KAEForm_viewBiz_pnlForm']/div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='กรอกหมายเลขโทรศัพท์มือถือไม่ถูกต้อง'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='มือถือ:'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='คำถามที่พบบ่อย'])[2]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ข้าพเจ้ายินยอมให้ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) และ/หรือ บริษัท อยุธยา แคปปิตอล ออโต้ ลีส จำกัด (มหาชน) ใช้ข้อมูลที่ข้าพเจ้าให้ไว้ข้างต้นในการนำเสนอผลิตภัณฑ์สินเชื่อยานยนต์ และ/หรือ ผลิตภัณฑ์ประกันภัยที่เห็นว่าเป็นประโยชน์แก่ข้าพเจ้าได้']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]</value>
   </webElementXpaths>
</WebElementEntity>
