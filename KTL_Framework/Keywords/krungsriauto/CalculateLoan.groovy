package krungsriauto

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class CalculateLoan {


	def WebKeyword web = new WebKeyword()
	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void LunchWebKrungsriauto(){

		util.FW_TestStartStep("เปิดเวปกรุงศรีคำนวนสินเชื่อ")
		web.FW_OpenBrowser("https://www.krungsriauto.com/auto/home.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(3, 1))
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsriauto/CheckElementopenWeb'), "ตรวจสอบชื่อแถบ “สมัครสินเชื่อรถยนต์ ”","สมัครสินเชื่อรถยนต์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickPreliminaryCreditCalculation(){

		util.FW_TestStartStep("คลิกปุ่ม คำนวณสินเชื่อเบื้องต้น")

		web.FW_Click(findTestObject('Object Repository/Krungsriauto/Btn Preliminary credit calculation'), "คำนวณสินเชื่อ")
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsriauto/Text Loan calculation'), "คำนวณสินเชื่อ", "คำนวณสินเชื่อ")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectLoanType(){

		util.FW_TestStartStep("เลือกประเภทสินเชื่อ")

		web.FW_Click(findTestObject('Krungsriauto/button_Loan_Type'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”")
		web.FW_Click(findTestObject('Krungsriauto/span_Loan_Type'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”")
		web.FW_SelectOptionByValue(findTestObject('Krungsriauto/select_Loan_Type'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”", 'mc')
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsriauto/filter loan type'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”", "กรุงศรี มอเตอร์ไซค์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}



	@Keyword
	def void InputCarprice(){

		util.FW_TestStartStep("กรอกราคารถ (ไม่รวม VAT)")
		web.FW_SetText(findTestObject("Object Repository/Krungsriauto/Car price"), "หน้าจอแสดงราคารถ “60,000 บาท”", "60,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Car price"), "หน้าจอแสดงราคารถ “60,000 บาท”", "60,000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void InputDownPayment(){

		util.FW_TestStartStep("กรอกเงินดาวน์")
		web.FW_SetText(findTestObject("Object Repository/Krungsriauto/Input Down payment"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”", "10,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input Down payment"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”", "10,000")
		web.FW_Click(findTestObject("Object Repository/Krungsriauto/Input Down percent"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input Down percent"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”", "16.67")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectInstallmentPeriod(){

		util.FW_TestStartStep("เลือก ระยะเวลาผ่อนชำระ ")
		web.FW_SelectOptionByValue(findTestObject("Object Repository/Krungsriauto/select_Installment period"), "หน้าจอแสดงระยะเวลาผ่อนชำระ “48”", "48")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/select_Installment period"), "หน้าจอแสดงระยะเวลาผ่อนชำระ “48”", "48")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void InterestRatePerMonth(){

		util.FW_TestStartStep("กรอกดอกเบี้ย")
		web.FW_SetText(findTestObject("Object Repository/Krungsriauto/Input_Interest rate per month"), "หน้าจอแสดงดอกเบี้ย “3.00 %”", "3")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input_Interest rate per month"), "หน้าจอแสดงดอกเบี้ย “3.00 %”", "3")
		web.FW_Click(findTestObject("Object Repository/Krungsriauto/Percent element"), "หน้าจอแสดงดอกเบี้ย “3.00 %”")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickCalculateloan(){

		util.FW_TestStartStep("กดปุ่มคำนวณสินเชื่อ")
		web.FW_Click(findTestObject("Object Repository/Krungsriauto/BtnCalculateloan"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input_BuyLoad"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "50,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/loan_per_month"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "2,542")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void VerifyInterestRatePerMonth(){

		util.FW_TestStartStep("ตรวจสอบยอดจัดเช่าซื้อ")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input_BuyLoad"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "50,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/loan_per_month"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "2,542")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	@Keyword

	def void ClosekrungsriAutoWebbrowser(){
		util.FW_TestStartStep("ปิดเวปกรุงศรีคำนวณสินเชื่อ")

		web.FW_CloseBrowser("https://www.krungsriauto.com/auto/Calculator.html")

		util.FW_TestEndStep()
	}



	@Keyword
	def void ClickRequestloan(){

		util.FW_TestStartStep("คลิกปุ่ม ขอสินเชื่อ")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/a_'), "ขอสินเชื่อรถยนต์")
		web.FW_CheckMessage(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/div_'), "ขอสินเชื่อรถยนต์", "ขอสินเชื่อรถยนต์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void RequestSelectLoanType(){

		util.FW_TestStartStep("เลือกประเภทสินเชื่อ")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span_'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span_ (1)'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”")
		//		WebUI.verifyTextPresent("กรุงศรี มอเตอร์ไซค์", false)
		web.FW_verifyTextPresent("หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”", findTestData('Data Files/TC005').getValue(1, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputName_Surname(){

		util.FW_TestStartStep("กรอกชื่อนามสกุล")

		web.FW_SetText(findTestObject('Object Repository/Krungsriauto/ขอสินเชื่อรถมอไซ/Page/inputNameLastnametxtText'), "หน้าจอแสดงชื่อ-นามสกุล “แทนไท ศรีภูมิ”", findTestData('Data Files/TC005').getValue(2, 1))
		web.FW_CheckMessageBygetAttribute(findTestObject('Object Repository/Krungsriauto/ขอสินเชื่อรถมอไซ/Page/inputNameLastnametxtText'), "หน้าจอแสดงชื่อ-นามสกุล “แทนไท ศรีภูมิ”", 'value', findTestData('Data Files/TC005').getValue(2, 1))
		//		web.FW_CheckMessageBygetAttribute(findTestObject("Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input_-_pltctl03pageplaceholderpltctl02KAEF_ec7dba"), "หน้าจอแสดงชื่อ-นามสกุล “แทนไท ศรีภูมิ”", "Value", "แทนไท ศรีภูมิ")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectProvince(){

		util.FW_TestStartStep("กรอกจังหวัด (ที่อยู่ปัจจุบัน)")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span__1'), "หน้าจอแสดงชื่อ “จังหวัดชัยภูมิ “")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/a__1'), "หน้าจอแสดงชื่อ “จังหวัดชัยภูมิ “")
		web.FW_verifyTextPresent("หน้าจอแสดงชื่อ “จังหวัดชัยภูมิ “", findTestData('Data Files/TC005').getValue(3, 1))
		//		web.FW_SelectOptionByValue(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/select_ (1)'), "หน้าจอแสดงชื่อ “จังหวัดชัยภูมิ “", 'ชัยภูมิ')
		//		WebUI.sendKeys(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/select_ (1)'), Keys.chord(Keys.TAB))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectPostalCode(){

		util.FW_TestStartStep("กรอก รหัสไปรษณีย์")
		web.FW_Click(findTestObject('Object Repository/Krungsriauto/ขอสินเชื่อรถมอไซ/Page/PostCode'), "หน้าจอแสดงกรอก รหัสไปรษณีย์ “36000”")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/PostcodeSelect'), "หน้าจอแสดงกรอก รหัสไปรษณีย์ “36000”")
		web.FW_verifyTextPresent("หน้าจอแสดงกรอก รหัสไปรษณีย์ “36000”", findTestData('Data Files/TC005').getValue(4, 1))
		//		web.FW_SelectOptionByValue(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/select_360003611036120361303614036150361603_cd0e57'), "หน้าจอแสดงกรอก รหัสไปรษณีย์ “36000”", "36000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()

		//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span__1'))
		//
		//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/PostcodeSelect'))
		//
	}

	@Keyword
	def void InputMobilePhone(){

		util.FW_TestStartStep("กรอกมือถือ")
		web.FW_SetText(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__pltctl03pageplaceholderpltctl02KAEFo_a0de0c'), "หน้าจอแสดงมือถือ “0879340879”", findTestData('Data Files/TC005').getValue(5, 1))
		web.FW_CheckMessageBygetAttribute(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__pltctl03pageplaceholderpltctl02KAEFo_a0de0c'), "หน้าจอแสดงมือถือ “0879340879”", 'value', findTestData('Data Files/TC005').getValue(5, 1))
		//		web.FW_CheckMessage(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__pltctl03pageplaceholderpltctl02KAEFo_a0de0c'), "หน้าจอแสดงมือถือ “0879340879”", "0879340879")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}



	@Keyword
	def void SelectConsentToBank (){

		util.FW_TestStartStep("เลือกยินยอมให้ใช้ข้อมูลที่ข้าพเจ้าให้ไว้ข้างต้นในการนำเสนอผลิตภัณฑ์สินเชื่อยานยนต์ ")
		web.FW_Click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__pltctl03pageplaceholderpltctl02KAEFo_5a8fb5'), "ข้าพเจ้ายินยอมให้ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) และ/หรือ บริษัท อยุธยา แคปปิตอล ออโต้ ลีส จำกัด (มหาชน) ใช้ข้อมูลที่ข้าพเจ้าให้ไว้ข้างต้นในการนำเสนอผลิตภัณฑ์สินเชื่อยานยนต์ และ/หรือ ผลิตภัณฑ์ประกันภัยที่เห็นว่าเป็นประโยชน์แก่ข้าพเจ้าได้”")
		web.FW_verifyTextPresent("เลือกยินยอมให้ใช้ข้อมูลที่ข้าพเจ้าให้ไว้ข้างต้นในการนำเสนอผลิตภัณฑ์สินเชื่อยานยนต์ ", findTestData('Data Files/TC005').getValue(6, 1))
		//		web.FW_CheckMessage(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/div_  ()        ()'), "ข้าพเจ้ายินยอมให้ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) และ/หรือ บริษัท อยุธยา แคปปิตอล ออโต้ ลีส จำกัด (มหาชน) ใช้ข้อมูลที่ข้าพเจ้าให้ไว้ข้างต้นในการนำเสนอผลิตภัณฑ์สินเชื่อยานยนต์ และ/หรือ ผลิตภัณฑ์ประกันภัยที่เห็นว่าเป็นประโยชน์แก่ข้าพเจ้าได้”", "ความยินยอม")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void VerifyRequestloanButton (){

		util.FW_TestStartStep("ตรวจสอบปุ่ม สนใจขอสินเชื่อ")
		web.FW_CheckMessageBygetAttribute(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__KAShortEForm'), "หน้าจอแสง ตรวจสอบปุ่ม สนใจขอสินเชื่อ", "value", "สนใจขอสินเชื่อ")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/a_'))

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/div_'))

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span_'))

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span_ (1)'))

	//	WebUI.selectOptionByValue(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/select_'),
	//		'MC', true)

	////	WebUI.setText(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input_-_pltctl03pageplaceholderpltctl02KAEF_ec7dba'),
	//		'แทนไท ศรีภูมิ')
	//
	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span__1'))
	//
	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/a__1'))
	//
	////	WebUI.selectOptionByValue(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/select_ (1)'),
	//		'ชัยภูมิ', true)

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/span__1'))
	//
	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/PostcodeSelect'))
	//
	//	WebUI.selectOptionByValue(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/select_360003611036120361303614036150361603_cd0e57'),
	//		'36000', true)

	//	WebUI.setText(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__pltctl03pageplaceholderpltctl02KAEFo_a0de0c'),
	//		'0879340849')

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__pltctl03pageplaceholderpltctl02KAEFo_5a8fb5'))

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/div_  ()        ()'))

	//	WebUI.click(findTestObject('Krungsriauto/ขอสินเชื่อรถมอไซ/Page/input__KAShortEForm'))

	//	WebUI.click(findTestObject('Object Repository/Krungsriauto/ขอสินเชื่อรถมอไซ/Page_Thank you Page        -      -    -       -/a_'))



	//	@Keyword
	//	def void ClickPreliminaryCreditCalculation(){
	//
	//		util.FW_TestStartStep("คลิกปุ่ม คำนวณสินเชื่อเบื้องต้น")
	//		web.FW_
	//
	//		util.FW_CaptureScreenShot()
	//		util.FW_TestEndStep()
	//	}



}
