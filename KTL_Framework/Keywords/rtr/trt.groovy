package rtr

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class trt {
	def WebKeyword web = new WebKeyword()
	def UtilityKeyword util = new UtilityKeyword()


	@Keyword
	def void LunchWebKrungsrimarket(){

		util.FW_TestStartStep("เปิดเวปกรุงศรีคำนวนสินเชื่อ")
		web.FW_OpenBrowser("https://www.krungsrimarket.com/CalculateLoan")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(3, 1))
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/Calculated from car price Tab'), "ตรวจสอบชื่อแถบ “คำนวณจากราคารถยนต์”","คำนวณจากราคารถยนต์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
}
